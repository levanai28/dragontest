﻿namespace Web.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Net.Http;
    using System.Web.Script.Serialization;
    using System.Collections.Generic;

    public class GoogleApiService
    {
        private const string ApiUrl = "https://maps.googleapis.com/maps/api/geocode/json";
        private const string ApiKey = "AIzaSyA2FvAiwpNKyT5a-VKKFNVJ2wYdNErErGc";

        public async Task<string> GetAddressCoordinatesAsync(double[] coordinates)
        {

            try
            {
                //this._logger.InfoFormat("GetAddressCoordinatesAsync input: {0} Key: {1} TimeOut: {2}", string.Join(Constants.Comma, key, requestTimeout));
                if (coordinates != null)
                {
                    using (var httpClient = new HttpClient())
                    {

                        var requestUri = $"{ApiUrl}?latlng={coordinates[0]},{coordinates[1]}&language=iw&key={ApiKey}";
                        var res = await httpClient.GetStringAsync(requestUri);
                        var responseJson = new JavaScriptSerializer().Deserialize<RootObject>(res);

                        //this._logger.InfoFormat("GetAddressCoordinatesAsync response status: {0}", responseJson.Status);

                        // Possible Response Status {REQUEST_DENIED, ZERO_RESULTS, OK} (For Log)
                        if (responseJson.Status == "OK")
                        {
                            var address = responseJson.Results.FirstOrDefault().Formatted_Address;
                            return address;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //this._logger.ErrorFormat("GetAddressCoordinatesAsync error: {0}", ex.Message);
            }
            return null;
        }

        private class RootObject
        {
            public List<Result> Results { get; set; }
            public string Status { get; set; }
        }

        public class Result
        {
            public List<AddressComponent> Address_Components { get; set; }
            public string Formatted_Address { get; set; }
            public Geometry Geometry { get; set; }
            public bool Partial_Match { get; set; }
            public List<string> Types { get; set; }
        }

        public class AddressComponent
        {
            public string Long_Name { get; set; }
            public string Short_Name { get; set; }
            public List<string> Types { get; set; }
        }

        public class Geometry
        {
            public Bounds Bounds { get; set; }
            public Location Location { get; set; }
            public string Location_Type { get; set; }
            public Bounds Viewport { get; set; }
        }

        public class Bounds
        {
            public Location NorthEast { get; set; }
            public Location SouthWest { get; set; }
        }

        public class Location
        {
            public double Lat { get; set; }
            public double Lng { get; set; }
        }
    }
}