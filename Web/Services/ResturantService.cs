﻿
using DragonTest.Model;
using DragonTest.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Web.Services
{
    public class ResturantService
    {
        private readonly GoogleApiService _googleApiService = new GoogleApiService();
        public async Task <ResturantsVM> GetResturantsData(List<Resturant> resturants)
        {
            var resturantsVM = new ResturantsVM();
            resturantsVM.RestTypes = new ResturantsDAL().GetAllTypeResturants();
            
            if (resturants != null)
            {
                foreach (var rest in resturants)
                {
                    rest.Address = await _googleApiService.GetAddressCoordinatesAsync(rest.Cordination);
                }
            }
            resturantsVM.Resturants = resturants;
            return resturantsVM;
        }

        public bool AddResturat(Resturant resturant)
        {
            return true;
        }

        public async Task<ResturantsVM> GetAllResturants()
        {
            var resturants = new ResturantsDAL().GetAllResturants();
            var vm = await GetResturantsData(resturants);
            return vm;
        }  
        

        public async Task <Resturant> InsertResturant (Resturant rest)
        {
            Resturant newRest = null;
               var identNumber = new ResturantsDAL().InsertResturants(rest);
            if(identNumber > 0)
            {
                rest.Address = await _googleApiService.GetAddressCoordinatesAsync(rest.Cordination);
                newRest = rest;
                newRest.Id = identNumber;
            }
            return newRest;
        }
    }
}