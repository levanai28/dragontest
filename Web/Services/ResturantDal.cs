﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DragonTest.Model;

namespace Web.Services
{
    public class ResturantsDAL
    {
        public List<Tuple<int, string>> GetAllTypeResturants()
        {
            List<Tuple<int, string>> restType = new List<Tuple<int, string>>();
            System.Data.SqlClient.SqlConnection con;
            con = new System.Data.SqlClient.SqlConnection();

            con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringDragonSql"].ConnectionString;
            //con.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename = C:\ProjectsCode\DragonTest\Web\App_Data\resturantsDB.mdf; Integrated Security = True";

            string query = "select * from ResturantTypes";
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, con);
            con.Open();
            System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                Tuple<int, string> newType = new Tuple<int, string>(
                    Int32.Parse(reader["Id"].ToString()),
                     reader["Name"].ToString()
                    );


                restType.Add(newType);
            }

            con.Close();
            return restType;

        }




        public List<Resturant> GetAllResturants()
        {
            List<Resturant> resturants = new List<Resturant>();
            System.Data.SqlClient.SqlConnection con;
            con = new System.Data.SqlClient.SqlConnection();

            con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringDragonSql"].ConnectionString;
            //con.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename = C:\ProjectsCode\DragonTest\Web\App_Data\resturantsDB.mdf; Integrated Security = True";

            string query = "select * from Resturants";
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, con);
            con.Open();
            System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                var x = (double)reader["Latitude"];
                var y = (double)reader["Lotitude"];
                Resturant rest = new Resturant()
                {
                    Id = Int32.Parse(reader["Id"].ToString()),
                    Name = reader["RestName"].ToString(),
                    PhoneNumber = Int32.Parse(reader["Phone"].ToString()),
                    Cordination = new double[] { x, y },
                    Type = (Enums.RestType)Int32.Parse(reader["TypeId"].ToString())

                };

                resturants.Add(rest);
            }

            con.Close();
            return resturants;

        }

        public int InsertResturants(Resturant rest)
        {
            var identNumber = 0;
            System.Data.SqlClient.SqlConnection con;
            con = new System.Data.SqlClient.SqlConnection();
            con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringDragonSql"].ConnectionString;

            string query = "IF NOT EXISTS (SELECT 1 FROM Resturants WHERE Phone=@Phone AND RestName =@RestName and Latitude=@Latitude And Lotitude=@Lotitude) BEGIN  Insert into Resturants (RestName,Phone,Latitude,Lotitude,TypeId) values (@RestName,@Phone,@Latitude,@Lotitude,@TypeId) SELECT SCOPE_IDENTITY() End";
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, con);

            System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter();
            param.ParameterName = "@Phone";
            param.Value = rest.PhoneNumber;
            command.Parameters.Add(param);

            Enums.RestType restType = Enums.RestType.Asian;
            Enum.TryParse(rest.Type.ToString(), out restType);
            param = new System.Data.SqlClient.SqlParameter();
            param.ParameterName = "@TypeId";
            param.Value = (int)restType;
            command.Parameters.Add(param);
                       
            param = new System.Data.SqlClient.SqlParameter();
            param.ParameterName = "@RestName";
            param.Value = rest.Name;
            command.Parameters.Add(param);

            param = new System.Data.SqlClient.SqlParameter();
            param.ParameterName = "@Latitude";
            param.Value = rest.Cordination[0];
            command.Parameters.Add(param);

            param = new System.Data.SqlClient.SqlParameter();
            param.ParameterName = "@Lotitude";
            param.Value = rest.Cordination[1];
            command.Parameters.Add(param);

            con.Open();
            System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                identNumber = Int32.Parse(reader[0].ToString());
            }

            con.Close();
            return identNumber;

        }


        public bool DeleteResturants(int restId)
        {
            bool isSuccsess = false;
            System.Data.SqlClient.SqlConnection con;
            con = new System.Data.SqlClient.SqlConnection();
            con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringDragonSql"].ConnectionString;

            string query = " Delete FROM Resturants WHERE Id=@Id";
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, con);

            System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter();
            param.ParameterName = "@Id";
            param.Value = restId;
            command.Parameters.Add(param);
            con.Open();
            //System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();


            int rowsAffected = command.ExecuteNonQuery();
            if (rowsAffected > 0)
            {
                isSuccsess = true;
            }
                       
            con.Close();
            return isSuccsess;

        }


    }
}

