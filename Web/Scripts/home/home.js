﻿jQuery(document).ready(function () {
   
    var form = $("#form");
    form.validate({
        rules: {
            name: 'required',
            phone: 'required',
            type: 'required',
            latitude: 'required',
            longitud: 'required',
            phone: 'required',
        },
        messages: {
            name: 'This field is required',
            type: 'This field is required',
            latitude: 'This field is required',
            longitud: 'This field is required',
            phone: 'This field is required'
        },
        submitHandler: function () {
            addResturant();
        }
    });
       
    $("#searchInput").on("keyup", function () {
       
            var value = $(this).val().toLowerCase();
        $("#resturantTbl tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
    });

    deleteResturant();

});


var addResturant = function () {
   
    var name = $("#name").val();
    var type = $("#type").val();
    var latitude = $("#latitude").val();
    var longitud = $("#longitud").val();
    var phone = $("#phone").val();

    var cordination = [];
    cordination.push(latitude);
    cordination.push(longitud);

    var rest = {
        Name: $("#name").val(),
        PhoneNumber: $("#phone").val(),
        Type: $("#type").val(),
        Cordination: cordination,
        Address: "",
    };

    $.ajax({
        type: "POST",
        data: rest,
        url: "/Home/Create",
        dataType: "html",
        success: function (data) {
            if (data != "") {
                $('#resturantTbl > tbody:last-child').append(data);
            }
            else {
                $("#dialog").dialog();
            }        

        }
    });
}


var deleteResturant = function () {
   
    $(".deleteRestrant").on("click", function () { 

        var restId = $(this).data("id"); 
        var rowToDelete = $(this).closest('tr');
        
    $.ajax({
        type: "GET",       
        url: "/Home/DeleteResturant?restId=" + restId,
        dataType: "html",
        success: function (data) {
            
            if (data == "true") {
                (rowToDelete).remove();
            }
            else {
                $("#dialog").dialog();
            }
        }
    });
    });
}