﻿

namespace Web.Controllers
{
    using DragonTest.Model;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Web.Services;
    

    public class HomeController : Controller
    {
        private readonly ResturantService _resturantService = new ResturantService();
        // GET: Index
        public async Task<ActionResult> Index()
        {
            var vm = await _resturantService.GetAllResturants();           
            return View(vm);
        }

        
        [HttpPost]
        public async Task<ActionResult> Create(Resturant rest)
        {
            var result  = await _resturantService.InsertResturant(rest);
            if(result!= null)
            {
                return PartialView("~/Views/Partial/ResturantInfo/_ResturantRowData.cshtml", rest);
            }
            else
            {
                return null;
            }
        }

        [HttpGet]
        public ActionResult DeleteResturant(int restId)
        {
            var result =  new ResturantsDAL().DeleteResturants(restId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
