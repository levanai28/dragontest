﻿namespace Web.Tests
{

    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Threading.Tasks;
    using Web.Services;

    [TestClass]
    public class GoogleApiServiceTests
    {
        private readonly GoogleApiService _googleApiService;
        public GoogleApiServiceTests()
        {
            this._googleApiService = new GoogleApiService();
        }


        [TestMethod]
        public async Task GetAddressByCoordinatesWillReturnRamatGan()
        {
            var lotLon = new double[] { 32.096011, 34.821720 };
            var result = await this._googleApiService.GetAddressCoordinatesAsync(lotLon);
            Assert.AreEqual(result, "בן גוריון 2, מגדל ב.ס.ר 1, רמת גן, ישראל");
        }

    }
}