﻿namespace DragonTest.Model
{
    public class Resturant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Enums.RestType Type { get; set; }
        public string Phone { get; set; }
        public int PhoneNumber { get; set; }
        public double[] Cordination { get; set; }

        public string  Address { get; set; }
    }
       
}
